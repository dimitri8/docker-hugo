FROM alpine:latest

LABEL "name"="docker-hugo"
LABEL "maintainer"="Vupar <web@vupar.fr>"
LABEL "version"="0.1.0"
LABEL "repository"="https://gitlab.com/dimitri8/docker-hugo"
LABEL "homepage"="https://www.vupar.fr/"

LABEL "com.github.actions.name"="Hugo GitHub Action"
LABEL "com.github.actions.description"=""
LABEL "com.github.actions.icon"="upload-cloud"
LABEL "com.github.actions.color"="black"

#COPY README.md entrypoint.sh /

RUN apk update && apk add \
	hugo \
    nodejs \
    npm \
    && rm -rf /var/cache/apk/*

RUN chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]